// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
use crate::config;
use crate::karapulse::Message;
use failure::Error;
use gdk;
use gdk::prelude::*;
use get_if_addrs::get_if_addrs;
use gettextrs::*;
use glib::Sender;
use gstreamer as gst;
use gstreamer::prelude::*;
use gtk::prelude::*;
use std::cell::{Cell, RefCell};
use std::path::Path;
use url::Url;

trait PipelineAbs {
    fn get_pipeline(&self) -> &gst::Element;
    fn set_source(&self, media: &Path) -> Result<(), Error>;
    fn set_video_filter(&self, filter: &str) -> Result<(), Error>;
    fn release_video_sink(&self) -> Result<(), Error>;
}

impl dyn PipelineAbs {
    fn watch_bus(&self, tx: Sender<Message>) {
        let bus = self.get_pipeline().get_bus().unwrap();
        bus.add_watch_local(move |_, msg| {
            match msg.view() {
                gst::MessageView::Eos(..) => {
                    debug!("Reached EOS");
                    tx.send(Message::PlayingDone).unwrap();
                }
                gst::MessageView::Error(err) => {
                    error!(
                        "Error from {:?}: {} ({:?})",
                        err.get_src().map(|s| s.get_path_string()),
                        err.get_error(),
                        err.get_debug()
                    );
                }
                _ => (),
            };

            glib::Continue(true)
        })
        .expect("Failed to add bus watch");
    }

    fn destroy_pipeline(&self) {
        let pipeline = self.get_pipeline();
        let bus = pipeline.get_bus().unwrap();
        bus.remove_watch().unwrap();
        pipeline.set_state(gst::State::Null).unwrap();
    }

    fn set_state(&self, state: gst::State) -> Result<(), Error> {
        debug!("set pipeline to {:?}", state);
        self.get_pipeline().set_state(state)?;
        Ok(())
    }

    fn get_state(&self) -> gst::State {
        let (_, state, _) = self.get_pipeline().get_state(gst::ClockTime::none());
        state
    }

    fn get_position(&self) -> Option<u64> {
        let pipeline = self.get_pipeline();

        if let Some(pos) = pipeline.query_position::<gst::ClockTime>() {
            pos.seconds()
        } else {
            None
        }
    }
}

struct PipelinePlaybin {
    playbin: gst::Element,
}

impl PipelinePlaybin {
    fn new(video_sink: &gst::Element, tx: Sender<Message>) -> Result<Self, Error> {
        let playbin =
            gst::ElementFactory::make("playbin", None).map_err(|_| MissingElement("playbin"))?;

        playbin.set_property("video-sink", &video_sink)?;

        let playbin = Self { playbin };
        PipelineAbs::watch_bus(&playbin, tx);

        Ok(playbin)
    }
}

impl PipelineAbs for PipelinePlaybin {
    fn get_pipeline(&self) -> &gst::Element {
        &self.playbin
    }

    fn set_source(&self, media: &Path) -> Result<(), Error> {
        self.playbin.set_state(gst::State::Null)?;

        let path = media.canonicalize()?;
        let uri = Url::from_file_path(path.to_str().unwrap()).unwrap();
        self.playbin.set_property("uri", &uri.to_string())?;

        Ok(())
    }

    fn set_video_filter(&self, filter: &str) -> Result<(), Error> {
        let filter = gst::parse_bin_from_description(&filter, true)?;
        self.playbin.set_property("video-filter", &filter)?;
        Ok(())
    }

    fn release_video_sink(&self) -> Result<(), Error> {
        let none: Option<gst::Element> = None;
        self.playbin.set_property("video-sink", &none)?;
        self.playbin.set_state(gst::State::Null)?;
        Ok(())
    }
}

impl Drop for PipelinePlaybin {
    fn drop(&mut self) {
        PipelineAbs::destroy_pipeline(self);
    }
}

struct PipelineCdg {
    pipeline: gst::Element,
    filesrc_audio: gst::Element,
    filesrc_cdg: gst::Element,
    video_sink: gst::Element,
}

impl PipelineCdg {
    fn new(video_sink: &gst::Element, tx: Sender<Message>) -> Result<Self, Error> {
        let desc = "filesrc name=filesrc_audio ! decodebin ! playsink name=sink filesrc name=filesrc_cdg ! cdgparse ! cdgdec ! queue ! videoconvert ! textoverlay name=textoverlay shaded-background=true valignment=top halignment=right ! videoconvert name=videoconvert";
        let pipeline = gst::parse_launch(&desc)?;
        let bin = pipeline.downcast_ref::<gst::Bin>().unwrap();

        let filesrc_audio = bin.get_by_name("filesrc_audio").unwrap();
        let filesrc_cdg = bin.get_by_name("filesrc_cdg").unwrap();
        let videoconvert = bin.get_by_name("videoconvert").unwrap();
        let textoverlay = bin.get_by_name("textoverlay").unwrap();

        textoverlay.set_property("text", &get_overlay_text())?;

        bin.add(&video_sink.clone())?;
        videoconvert.link(video_sink)?;

        let pipeline_cdg = Self {
            pipeline,
            filesrc_audio,
            filesrc_cdg,
            video_sink: video_sink.clone(),
        };
        PipelineAbs::watch_bus(&pipeline_cdg, tx);

        Ok(pipeline_cdg)
    }
}

impl PipelineAbs for PipelineCdg {
    fn get_pipeline(&self) -> &gst::Element {
        &self.pipeline
    }

    fn set_source(&self, media: &Path) -> Result<(), Error> {
        self.pipeline.set_state(gst::State::Null)?;

        let path = media.canonicalize()?;
        self.filesrc_audio
            .set_property("location", &path.to_str())?;
        let path = path.with_extension("cdg");
        self.filesrc_cdg.set_property("location", &path.to_str())?;

        Ok(())
    }

    fn set_video_filter(&self, _filter: &str) -> Result<(), Error> {
        /* always using same filter */
        Ok(())
    }

    fn release_video_sink(&self) -> Result<(), Error> {
        let bin = self.pipeline.downcast_ref::<gst::Bin>().unwrap();
        let videoconvert = bin.get_by_name("videoconvert").unwrap();
        videoconvert.unlink(&self.video_sink);
        bin.remove(&self.video_sink)?;
        self.video_sink.set_state(gst::State::Null)?;
        Ok(())
    }
}

impl Drop for PipelineCdg {
    fn drop(&mut self) {
        PipelineAbs::destroy_pipeline(self);
    }
}

pub struct Player {
    pipeline: RefCell<Box<dyn PipelineAbs>>,
    video_sink: gst::Element,
    mode: Cell<Mode>,
    tx: Sender<Message>,
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum Mode {
    Playbin,
    Background,
    Cdg,
}

#[derive(Debug, PartialEq)]
pub enum State {
    Waiting,
    Playing,
    Paused,
}

fn get_overlay_text() -> String {
    // Ignore loopback and virtual interfaces
    // FIXME: don't hardcode port
    get_if_addrs()
        .unwrap()
        .iter()
        .filter(|iface| !iface.name.starts_with("virbr"))
        .map(get_if_addrs::Interface::ip)
        .filter(|ip| !ip.is_loopback())
        .filter(|ip| ip.is_ipv4())
        .map(|ip| format!("http://{}:1848", ip.to_string()))
        .collect::<Vec<String>>()
        .join("\n")
}

#[derive(Debug, Fail)]
#[fail(display = "Missing element {}", _0)]
struct MissingElement(&'static str);

impl Player {
    pub fn new(window: gtk::Window, tx: Sender<Message>) -> Result<Player, Error> {
        // Disable vaapi as it breaks rendering with gtkglsink
        let registry = gst::Registry::get();
        let plugin = registry.lookup("libgstvaapi.so");
        if let Some(plugin) = plugin {
            registry.remove_plugin(&plugin);
        }

        let gtkglsink = gst::ElementFactory::make("gtkglsink", None)
            .map_err(|_| MissingElement("gtkglsink"))?;
        let glsinkbin = gst::ElementFactory::make("glsinkbin", None)
            .map_err(|_| MissingElement("glsinkbin"))?;
        glsinkbin.set_property("sink", &gtkglsink)?;

        let playbin = PipelinePlaybin::new(&glsinkbin, tx.clone())?;

        /* Window */
        let widget = gtkglsink.get_property("widget")?;
        let widget = widget.get::<gtk::Widget>().unwrap().unwrap();

        // Hide cursor in fullscreen
        window.connect_window_state_event(|window, event| {
            let change = event.get_changed_mask();
            if change.contains(gdk::WindowState::FULLSCREEN) {
                let new_state = event.get_new_window_state();
                let win = window.get_window().unwrap();

                let cursor = if new_state.contains(gdk::WindowState::FULLSCREEN) {
                    gdk::Cursor::new(gdk::CursorType::BlankCursor)
                } else {
                    gdk::Cursor::new(gdk::CursorType::Arrow)
                };

                win.set_cursor(Some(&cursor));
            }
            Inhibit(false)
        });

        window.set_size_request(800, 600);
        window.fullscreen();
        window.add(&widget);
        window.show_all();

        window.connect_key_press_event(move |window, key| {
            let keyval = key.get_keyval();
            match keyval {
                gdk::enums::key::f => {
                    let win = window.get_window().unwrap();
                    let state = win.get_state();
                    if state.contains(gdk::WindowState::FULLSCREEN) {
                        window.unfullscreen();
                    } else {
                        window.fullscreen();
                    }
                }
                gdk::enums::key::Escape => window.unfullscreen(),
                _ => {}
            }
            Inhibit(false)
        });

        let player = Player {
            pipeline: RefCell::new(Box::new(playbin)),
            video_sink: glsinkbin,
            mode: Cell::new(Mode::Background),
            tx,
        };

        Ok(player)
    }

    fn start_pipeline(&self, media: &Path, video_filter: &str) -> Result<(), Error> {
        debug!("opening {}", media.display());
        {
            let pipeline = self.pipeline.borrow();
            pipeline.set_source(media)?;
            pipeline.set_video_filter(video_filter)?;
        }
        self.play()
    }

    fn switch_to_playbin(&self) -> Result<(), Error> {
        let mode = self.mode.get();
        if mode == Mode::Background || mode == Mode::Playbin {
            return Ok(());
        }

        {
            let pipeline = self.pipeline.borrow();
            pipeline.release_video_sink()?;
        }
        self.pipeline.replace(Box::new(PipelinePlaybin::new(
            &self.video_sink,
            self.tx.clone(),
        )?));

        Ok(())
    }

    fn display_background(&self) -> Result<(), Error> {
        self.switch_to_playbin()?;

        debug!("Display background");
        let path = Path::new(config::PKGDATADIR)
            .join("background.png")
            .canonicalize()?;
        let txt = format!(
            "{}\n{}",
            gettext("Waiting for songs").as_str(),
            get_overlay_text()
        );
        let desc = format!("textoverlay text=\"{}\" shaded-background=true valignment=center halignment=center ! imagefreeze", txt);

        self.mode.set(Mode::Background);
        self.start_pipeline(&path, &desc)
    }

    fn open_video(&self, media: &Path) -> Result<(), Error> {
        self.switch_to_playbin()?;

        let desc = format!(
            "textoverlay text=\"{}\" shaded-background=true valignment=top halignment=right",
            get_overlay_text()
        );

        self.mode.set(Mode::Playbin);
        self.start_pipeline(&media, &desc)
    }

    fn open_cdg(&self, media: &Path) -> Result<(), Error> {
        {
            let pipeline = self.pipeline.borrow();
            pipeline.release_video_sink()?;
        }
        /* FIXME: recycle existing cdg pipeline */
        self.pipeline.replace(Box::new(PipelineCdg::new(
            &self.video_sink,
            self.tx.clone(),
        )?));

        self.mode.set(Mode::Cdg);
        self.start_pipeline(&media, &get_overlay_text())
    }

    pub fn open_media(&self, media: &Path) -> Result<(), Error> {
        // Assume mp3 file have a CDG associated
        if let Some(ext) = media.extension() {
            if ext == "mp3" {
                return self.open_cdg(media);
            }
        }
        self.open_video(media)
    }

    pub fn get_state(&self) -> Result<State, Error> {
        if self.mode.get() == Mode::Background {
            return Ok(State::Waiting);
        }

        let pipeline = self.pipeline.borrow();
        let state = pipeline.get_state();
        match state {
            gst::State::Playing => Ok(State::Playing),
            gst::State::Null => Ok(State::Waiting),
            _ => Ok(State::Paused),
        }
    }

    pub fn pause(&self) -> Result<(), Error> {
        let pipeline = self.pipeline.borrow_mut();
        pipeline.set_state(gst::State::Paused)
    }

    pub fn play(&self) -> Result<(), Error> {
        let pipeline = self.pipeline.borrow_mut();
        pipeline.set_state(gst::State::Playing)
    }

    pub fn stop(&self) -> Result<(), Error> {
        self.display_background()
    }

    pub fn get_position(&self) -> Option<u64> {
        let pipeline = self.pipeline.borrow();
        pipeline.get_position()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::common::init;
    use crate::tests::TestMedia;
    use std::{thread, time};

    struct Test {
        player: Player,
    }

    impl Test {
        fn new() -> Test {
            let _ = env_logger::try_init();
            init().unwrap();

            let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);

            rx.attach(None, move |msg| {
                match msg {
                    Message::PlayingDone => gtk::main_quit(),
                    _ => {}
                }
                glib::Continue(true)
            });

            let window = gtk::Window::new(gtk::WindowType::Toplevel);
            Test {
                player: Player::new(window, tx).unwrap(),
            }
        }

        fn open_media(&mut self, media: TestMedia) {
            self.player.open_media(&media.path()).unwrap();
        }

        fn run_until_eos(&self) {
            gtk::main();
        }
    }

    #[test]
    #[ignore]
    fn play_twice() {
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        assert_eq!(test.player.get_position(), None);
        test.run_until_eos();
        assert_eq!(test.player.get_position(), Some(2));

        test.open_media(TestMedia::Video2s);
        test.run_until_eos();
    }

    #[test]
    #[ignore]
    fn play_next() {
        let mut test = Test::new();

        assert_eq!(test.player.get_state().unwrap(), State::Waiting);

        test.open_media(TestMedia::Video2s);
        assert_eq!(test.player.get_state().unwrap(), State::Playing);
        thread::sleep(time::Duration::from_millis(500));

        test.open_media(TestMedia::Video2_2s);
        test.run_until_eos();
    }

    #[test]
    #[ignore]
    fn pause() {
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        assert_eq!(test.player.get_state().unwrap(), State::Playing);
        thread::sleep(time::Duration::from_millis(500));

        test.player.pause().unwrap();
        thread::sleep(time::Duration::from_millis(500));
        test.player.play().unwrap();
        test.run_until_eos();
    }

    #[test]
    #[ignore]
    fn stop() {
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        assert_eq!(test.player.get_state().unwrap(), State::Playing);
        test.player.stop().unwrap();
        assert_eq!(test.player.get_state().unwrap(), State::Waiting);
    }

    #[test]
    #[ignore]
    fn play_cdg() {
        let mut test = Test::new();

        test.open_media(TestMedia::Audio);
        test.run_until_eos();

        test.open_media(TestMedia::Audio);
        test.run_until_eos();
    }

    #[test]
    #[ignore]
    fn play_video_then_cdg() {
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        test.run_until_eos();

        test.open_media(TestMedia::Audio);
        test.run_until_eos();
    }

    #[test]
    #[ignore]
    fn play_cdg_then_video() {
        let mut test = Test::new();

        test.open_media(TestMedia::Audio);
        test.run_until_eos();

        test.open_media(TestMedia::Video2s);
        test.run_until_eos();
    }
}
