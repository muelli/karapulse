// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

// Structures returned as JSON reply. Be careful to not change their serialization
// as that would be a protocol break.
use serde::Serialize;

use crate::db::{History, Song};
use crate::karapulse::State;
use crate::queue::QueueItem;

// search
#[derive(Debug, Serialize)]
struct SearchResponseItem(Song);

#[derive(Debug, Serialize)]
pub struct SearchResponse(Vec<SearchResponseItem>);

impl SearchResponse {
    pub fn new(songs: Vec<Song>) -> Self {
        Self(songs.into_iter().map(SearchResponseItem).collect())
    }
}

// history
#[derive(Debug, Serialize)]
struct HistoryResponseItem(History);

#[derive(Debug, Serialize)]
pub struct HistoryResponse(Vec<HistoryResponseItem>);

impl HistoryResponse {
    pub fn new(history: Vec<History>) -> Self {
        Self(history.into_iter().map(HistoryResponseItem).collect())
    }
}

// status
#[derive(Debug, Serialize)]
pub struct StatusResponseSong {
    #[serde(flatten)]
    item: QueueItem,
    #[serde(skip_serializing_if = "Option::is_none")]
    eta: Option<u32>,
}

impl StatusResponseSong {
    pub fn new(item: QueueItem, eta: Option<u32>) -> Self {
        Self { item, eta }
    }
}

#[derive(Debug, Serialize)]
pub struct StatusResponse {
    pub state: State,
    pub current_song: Option<StatusResponseSong>,
    pub position: Option<u64>,
    pub queue: Vec<StatusResponseSong>,
}

impl StatusResponse {
    pub fn new(
        state: State,
        current_song: Option<StatusResponseSong>,
        position: Option<u64>,
        queue: Vec<StatusResponseSong>,
    ) -> Self {
        Self {
            state,
            current_song,
            position,
            queue,
        }
    }
}
