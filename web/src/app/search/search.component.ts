import { Component, OnInit } from '@angular/core';
import {KaraokeService} from '../karaoke.service';
import {Song} from '../song';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  results: Song[];
  searchTerms$ = new Subject<string>();

  constructor(private karaokeService: KaraokeService) {
    this.karaokeService.search(this.searchTerms$)
      .subscribe(results => {
        this.results = results;
      });
  }

  ngOnInit() {

  }

  log(txt: string) {
    console.log(txt);
  }
}
